const axios = require("axios");
const querystring = require("querystring");
const jsdom = require("jsdom");
const sanitizeHtml = require("sanitize-html");
const sha1 = require('js-sha1');
var cookieparse = require("set-cookie-parser");
const { JSDOM } = jsdom;

const allowedHtml = {
    allowedTags: ["b", "i", "em", "strong", "a", "s"],
    allowedAttributes: {
        a: ["href"],
    },
    allowedIframeHostnames: [],
};

async function authenticated_request(url, cookies, path) {
    return new Promise((resolve, reject) => {
        var c = cookieparse.parse(cookies, { map: true });
        axios({
                method: "get",
                url: `${url}${path}`,
                headers: {
                    Cookie: `IServSession=${c["IServSession"].value}; IServSAT=${c["IServSAT"].value}`,
                    "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:80.0) Gecko/20100101 Firefox/80.0",
                },
            })
            .then(function(response) {
                resolve(response);
            })
            .catch(function(response) {
                reject(response);
            });
    });
}

function safe_html(raw) {
    var safe = sanitizeHtml(raw, allowedHtml);
    return Boolean(safe.trim()) ? safe : null;
}

function parse_plan(html) {
    const document = new JSDOM(html).window.document;

    // The table containing the actual plan
    const contentTable = document.querySelector(".mon_list").firstElementChild;

    // The headers of the contentTable
    var contentHeaderList = [
        "time",
        "subject",
        "teacher",
        "text",
        "course",
        "room",
    ];

    let header = document.querySelector(".mon_title").textContent.split(" ");
    let date = header[0];
    let week = header[header.length - 1];

    //Scrapping function start

    var currentClass = null;
    var parsedPlan = {};

    Array.from(contentTable.children)
        .slice(1)
        .forEach((row) => {
            if (row.children.length === 6) {
                var parsedRow = {};
                if (!parsedPlan[currentClass]) {
                    //console.log("test");
                    parsedPlan[currentClass] = [];
                }
                for (const [i, column] of Array.from(row.children).entries()) {
                    //console.log(contentHeaderList[i], column);
                    if (column.children.length > 0) {
                        parsedRow[contentHeaderList[i]] = safe_html(
                            column.innerHTML
                        );
                    } else {
                        parsedRow[contentHeaderList[i]] = safe_html(column.innerHTML);
                    }
                }
                parsedPlan[currentClass].push(parsedRow);
            } else if (row.children.length === 1) {
                currentClass = safe_html(row.children[0].innerHTML.split(" ")[0]);
            }
        });

    return { plan: parsedPlan, date, week };

    //Scrapping function end
}

async function login(url, username, password) {
    return new Promise((resolve) => {
        axios({
                method: "post",
                url: `${url}/iserv/app/login?target=%2Fiserv%2F`,
                maxRedirects: 0,
                validateStatus: function(status) {
                    return status >= 200 && status < 400;
                },
                data: querystring.stringify({
                    _username: username,
                    _password: password,
                }),
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded",
                    "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:80.0) Gecko/20100101 Firefox/80.0",
                    Accept: "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
                },
            })
            .then(function(response) {
                //console.log(response.headers);
                if (response.headers["set-cookie"]) {
                    resolve({ ok: true, cookies: response.headers["set-cookie"] });
                } else {
                    resolve({ ok: false, code: 401 });
                }
            })
            .catch(function(error) {
                resolve({ ok: false, code: 502 });
            });
    });
}

exports.handler = async function(event, context) {
    // Only allow POST for security
    if (event.httpMethod !== "POST") {
        return { statusCode: 405, body: "Method Not Allowed" };
    }
    const params = JSON.parse(event.body);
    console.log("--- Info ---")
    console.log(`SHA1 Server: ${sha1(params.url)}`)
    console.log(`SHA1 User: ${sha1(params.username)}`)
    try {
        let login_data = await login(params.url, params.username, params.password);
        if (login_data.ok === false) {
            let code_map = {
                401: "Authentication with the remote has failed.",
                502: "Remote server is not responding",
            }
            let code = login_data.code
            return {
                statusCode: code,
                headers: {
                    "content-type": "application/json",
                    "Access-Control-Allow-Origin": "*",
                },
                body: JSON.stringify({
                    code: login_data.code,
                    message: code_map[code],
                }),
            };
        }
        let encoded_path_next = params.pathNext ?
            encodeURI(params.pathNext) :
            "/iserv/plan/show/raw/02-Vertreter%20Sch%C3%BCler%20morgen/subst_002.htm";

        let encoded_path_today = params.pathToday ?
            encodeURI(params.pathToday) :
            "/iserv/plan/show/raw/01-Vertreter%20Sch%C3%BCler%20heute/subst_001.htm";

        let response_next = await authenticated_request(
            params.url,
            login_data.cookies,
            encoded_path_next
        );
        let response_today = await authenticated_request(
            params.url,
            login_data.cookies,
            encoded_path_today
        );

        var parsed_next = parse_plan(response_next.data);
        var parsed_today = parse_plan(response_today.data);

        return {
            statusCode: 200,
            headers: {
                "content-type": "application/json",
                "Access-Control-Allow-Origin": "*",
            },
            body: JSON.stringify([parsed_today, parsed_next]),
        };
    } catch (err) {
        console.log(err)
        return {
            statusCode: 500,
            body: JSON.stringify({ code: 500, message: "Server error" }), // Could be a custom message or object i.e. JSON.stringify(err)
        };
    }
};