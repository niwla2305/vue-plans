module.exports = {
    pwa: {
        name: 'VuePlans',
        themeColor: '#488cff',
        msTileColor: '#000000',
        appleMobileWebAppCapable: 'yes',
        appleMobileWebAppStatusBarStyle: 'black',
        iconPaths: {
            favicon32: 'img/icons/favicon-32x32.png',
            favicon16: 'img/icons/favicon-16x16.png',
            appleTouchIcon: 'img/icons/apple-touch-icon-152x152.png',
            maskIcon: null,
            msTileImage: 'img/icons/icon-144x144.png'
        }

    }
}