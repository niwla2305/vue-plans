export default function loadSettings({ next, router, store }) {
    // if (!localStorage.getItem('username') || !localStorage.getItem('courses')) {
    //     return router.push({ name: 'Config' });
    // }
    store.commit("updateUsername", localStorage.getItem("username"))
    store.commit("updateCourses", localStorage.getItem("courses")?.split(","))
    store.commit("updateIservURL", localStorage.getItem("iservURL"))
    return next()
}