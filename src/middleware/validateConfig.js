import Vuex from 'vuex'

export default function loadSettings({ next, router, store }) {
    if (!localStorage.getItem('username') || !localStorage.getItem('courses')) {
        return router.push({ name: 'Config' });
    }
    return next()
}