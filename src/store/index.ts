import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    iservURL: null,
    privacyURL: "/api",
    username: null,
    courses: null,
    password: null
  },
  mutations: {
    updateUsername(state, n) {
      state.username = n
    },
    updateIservURL(state, n) {
      state.iservURL = n
    },
    updateCourses(state, n) {
      state.courses = n
    },
    updatePassword(state, n) {
      state.password = n
    }
  },
  actions: {
  },
  modules: {
  }
})
